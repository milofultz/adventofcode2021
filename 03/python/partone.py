# Get data
import os.path


def get_input(name: str) -> list[str]:
    with open(os.path.join("..", name), 'r') as f:
        data = f.read()
    return [line for line in data.split('\n') if line != ""]


def get_gamma(nums: list[str]) -> str:
    counts = [0 for char in range(len(nums[0]))]
    # get most common bits for each position
    for num in nums:
        for index, char in enumerate(num):
            counts[index] += 1 if char == '1' else 0

    output = ""
    for count in counts:
        if count > (len(nums) / 2):
            output += "1"
        else:
            output += "0"
    # return most common bits
    return output


def get_epsilon(gamma: str) -> str:
    output = ""
    for num in gamma:
        if num == "1":
            output += "0"
        else:
            output += "1"
    return output


def get_power_consumption() -> int:
    input_data = get_input('in.raw')
    gamma = get_gamma(input_data)
    epsilon = get_epsilon(gamma)
    return int(gamma, 2) * int(epsilon, 2)


if __name__ == "__main__":
    print(get_power_consumption())
