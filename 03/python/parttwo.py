from partone import get_input

# Oxygen Generator Rating
# I: Array of binary numbers
# O: Array of binary numbers (that have MOST common value as bit)

def get_oxygen(nums: list[str]) -> str:
    nums_to_search = nums
    for bit in range(len(nums[0])):
        oxygen_nums = {
            "0": list(),
            "1": list(),
        }
        if len(nums_to_search) == 1:
            break
        for num in nums_to_search:
            oxygen_nums[num[bit]].append(num)
        if len(oxygen_nums["0"]) > len(oxygen_nums["1"]):
            nums_to_search = oxygen_nums["0"]
        else:
            nums_to_search = oxygen_nums["1"]
    return nums_to_search[0]


# CO2 Scrubber Rating
# I: Array of binary numbers
# O: Array of binary numbers (that have LEAST common value as bit)

def get_co2(nums: list[str]) -> str:
    nums_to_search = nums
    for bit in range(len(nums[0])):
        co2_nums = {
            "0": list(),
            "1": list(),
        }
        if len(nums_to_search) == 1:
            break
        for num in nums_to_search:
            co2_nums[num[bit]].append(num)
        if len(co2_nums["1"]) < len(co2_nums["0"]):
            nums_to_search = co2_nums["1"]
        else:
            nums_to_search = co2_nums["0"]
    return nums_to_search[0]

# Bit Criteria
# I: Array of binary numbers
# O: 2 binaries (Oxygen generator, CO2 scrubber)

def get_bit_criteria(nums: list[str]) -> (str, str):
    oxygen = get_oxygen(nums)
    co2 = get_co2(nums)
    return oxygen, co2

# Life Support Rating
# I: 2 binaries (Oxygen generator, CO2 scrubber)
# O: Product of both nums

def get_life_support() -> int:
    input_data = get_input('in.raw')
    oxygen, co2 = get_bit_criteria(input_data)
    print(oxygen, co2)
    return int(oxygen, 2) * int(co2, 2)


if __name__ == '__main__':
    print(get_life_support())
